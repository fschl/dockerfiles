# Start Developing with Cordova

1. start Cordova container
   ```bash
   docker run -it \
           --name="corova-cli" \
           --net host \
           -v $HOME/projects/cordova/:/src/ \
           -v /dev/bus/usb:/dev/bus/usb \
           -v /tmp/.X11-unix:/tmp/.X11-unix \
           -e DISPLAY=unix$DISPLAY \
           fschl/cordova bash
    ```

    https://cordova.apache.org/docs/en/latest/guide/cli/index.html
2. `cordova create hello de.fschl.hello HelloWorld --template cordova-app-hello-world`
find templates at https://www.npmjs.com/search?q=cordova:template
3. `cd hello`
4. `cordova platform add browser android`
5. verify everything is instlaled `cordova requirements`
6. cordova build

## push and test on Android

1. out of container goto `./platforms/android/app/build/outputs/apk/debug`
2. `adb push app-debug.apk /sdcard/`
3. install + open app on your device

## push and test in browser

1. `cordova run browser`
2. visit http://127.0.0.1:8000/index.html
